package io.classpath.springbootdemo.service;

import io.classpath.springbootdemo.model.Student;
import io.classpath.springbootdemo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    @Override
    public Student save(Student student) {
        return this.studentRepository.save(student);
    }

    @Override
    public List<Student> findAll() {
        return this.studentRepository.findAll();
    }

    @Override
    public Student updateById(long id, Student student) {
        Optional<Student> studentOptional = this.studentRepository.findById(id);
        if (studentOptional.isPresent()){
            Student existingStudent = studentOptional.get();
            return this.studentRepository.save(existingStudent);
        }
        return student;
    }

    @Override
    public Student findById(long studentId) {
        return this.studentRepository.findById(studentId).orElseThrow(() -> new IllegalArgumentException(" Invalid Student Id"));
    }

    @Override
    public void deleteById(long studentId) {
        this.studentRepository.deleteById(studentId);
    }
}