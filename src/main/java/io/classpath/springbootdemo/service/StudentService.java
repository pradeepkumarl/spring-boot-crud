package io.classpath.springbootdemo.service;

import io.classpath.springbootdemo.model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Student save(Student student);

    List<Student> findAll();

    Student updateById(long id, Student student);

    Student findById(long studentId);

    void deleteById(long studentId);
}