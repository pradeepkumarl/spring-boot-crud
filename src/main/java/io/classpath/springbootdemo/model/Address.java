package io.classpath.springbootdemo.model;

import javax.persistence.*;

@Entity
@Table(name="address")
public class Address {

    @Id
    @GeneratedValue
    private int addressId;

    private String city;

    private String state;

    @ManyToOne
    @JoinColumn(name="student_id")
    private Student student;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}