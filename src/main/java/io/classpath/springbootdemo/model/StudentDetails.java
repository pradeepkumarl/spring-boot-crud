package io.classpath.springbootdemo.model;

import javax.persistence.*;

@Entity
@Table(name="student_details")
public class StudentDetails {

    @Id
    @GeneratedValue
    @Column(name="id")
    private long studentDetailsId;

    private String studentName;

    private int grade;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;

    public long getStudentDetailsId() {
        return studentDetailsId;
    }

    public void setStudentDetailsId(long studentDetailsId) {
        this.studentDetailsId = studentDetailsId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}