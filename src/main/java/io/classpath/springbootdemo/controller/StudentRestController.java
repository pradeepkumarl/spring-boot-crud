package io.classpath.springbootdemo.controller;

import io.classpath.springbootdemo.model.Address;
import io.classpath.springbootdemo.model.Student;
import io.classpath.springbootdemo.model.StudentDetails;
import io.classpath.springbootdemo.service.StudentService;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
public class StudentRestController {

    private final StudentService studentService;

    public StudentRestController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> fetchAll(){
        return this.studentService.findAll();
    }

    @GetMapping("/{id}")
    public Student fetchById(@PathVariable long id){
        return this.studentService.findById(id);
    }

    @PostMapping
    public Student save(@RequestBody Student student){
       student.getAddressList().forEach(address -> address.setStudent(student));
        student.getStudentDetails().setStudent(student);

/*
        Student student = new Student();
        StudentDetails studentDetails = new StudentDetails();
        studentDetails.setStudentName("Kishore");
        studentDetails.setGrade(5);

        Address address1 = new Address();
        address1.setCity("Bangalore");
        address1.setState("Karnataka");

        Address address2 = new Address();
        address2.setCity("Chennai");
        address2.setState("TamilNadu");

        Address address3 = new Address();
        address3.setCity("Bangalore");
        address3.setState("Karnataka");

        Address address4 = new Address();
        address4.setCity("Chennai");
        address4.setState("TamilNadu");

        address1.setStudent(student);
        address2.setStudent(student);

        studentDetails.setStudent(student);
        student.setStudentDetails(studentDetails);

        student.setAddressList(Arrays.asList(address1, address2));
*/
        return
                this.studentService.save(student);
    }

    @DeleteMapping("/{id}")
    public void deleteStudentById(@PathVariable long id){
        this.studentService.deleteById(id);
    }
}